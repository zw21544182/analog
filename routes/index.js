var express = require('express');
var router = express.Router();
var dataArray = require('../util/config');

function routerCallBack(req, res, next, item) {
    res.status(item.status);
    //如果状态码为400 返回统一自定义错误
    if (item.status === 400) {
        res.json({
            responseCode: -1,
            responseMessage: '此错误为模拟请求错误',
            data: null
        });
        return;
    }
    //如果状态码为500 返回统一自定义错误
    if (item.status === 500) {
        res.json({
            responseCode: -1,
            responseMessage: '此错误为模拟接口报错',
            data: null
        });
        return;
    }
    res.json(item.response);
}

dataArray.forEach(item => {
    if (item.method !== undefined && item.method === 'post') {
        router.post(item.path, function (req, res, next) {
            routerCallBack(req, res, next, item)
        })
    } else {
        router.get(item.path, function (req, res, next) {
            routerCallBack(req, res, next, item)
        });
    }
})

module.exports = router;
