/*
* 路由配置文件
* method 取值  get   post
* path 路由地址
* status 返回状态码
* response 返回json内容
* */
module.exports = [
    {
        method: 'get',
        path: '/invoice/invoiceOrder/list',
        status: 200,
        response:
            {
                "responseCode": 0,
                "responseMessage": "成功",
                "data": {
                    "orderInfos": [{
                        "orderId": "7454671230101132S5I0VB9OHVH",
                        "stationName": "xxx",
                        "startChargeTime": "2020-05-22 14:40:00",
                        "totalFee": 18,
                        "paidAmount": 2,
                        "discountAmount": 16,
                        "invoiceAmount": 17
                    }]
                }
            }
    }
    , {
        method: 'get',
        path: '/invoice/order/list',
        status: 400,
        response:
            {
                "responseCode": 0,
                "responseMessage": "成功",
                "data": {
                    "totalCount": 100,
                    "totalAmount": 9999,
                    "orderInfos": [{
                        "id": "7454671230101132S5I0VB9OHVH",
                        "stationName": "xxx",
                        "startChargeTime": "2020-05-22 14:40:00",
                        "sendStartChargeTime": "2020-05-22 14:40:00",
                        "totalFee": 18,
                        "paidAmount": 16,
                        "discountAmount": 2,
                        "invoiceAmount": 17
                    }]
                }
            }
    }
]